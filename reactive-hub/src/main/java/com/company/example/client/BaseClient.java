package com.company.example.client;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseClient {

    private final WebClient client;

    protected BaseClient(String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    protected Mono<ClientResponse> getClient(String uri, MediaType mediaType) {
        return client.get()
            .uri(uri)
            .headers(cons -> cons.setAll(createHeaders()))
            .accept(mediaType)
            .exchange();
//            .timeout(Duration.ofSeconds(10));
//            .take(Duration.ofSeconds(10));
    }

    private URI buildQuery(UriBuilder uriBuilder, String uri, Map<String, String> variables) {
        uriBuilder.path(uri);
        variables.forEach(uriBuilder::queryParam);
        return uriBuilder.build();
    }

    protected Map<String, String> createHeaders() {
        Map<String, String> headers = new HashMap<>();
        return headers;
    }

    protected Map<String, String> createQueryParams() {
        Map<String, String> query = new HashMap<>();
        return query;
    }

}
