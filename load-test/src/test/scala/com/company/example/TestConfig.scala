package com.company.example

import org.slf4j.LoggerFactory

import scala.sys.SystemProperties

object TestConfig {

  val props = new SystemProperties
  val baseUrl: String = "http://localhost:8080"
  val basePath: String = "/api"
  val maxUsers: Int = 100
  val rampTimeSeconds: Int = 20
  val loadTimeSeconds: Int = 10
  val maxLoadTime: Int = 60000

  {
    val logger = LoggerFactory.getLogger(getClass)
    logger.info("Hitting: {}", baseUrl + basePath)
    logger.info("Max Users: {}", maxUsers)
    logger.info("Ramp Up Seconds: {}", rampTimeSeconds)
    logger.info("Sustained Load Seconds: {}", loadTimeSeconds)
  }

}
