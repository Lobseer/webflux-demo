package com.company.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AwsS3ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsS3ServiceApplication.class, args);
    }

}
