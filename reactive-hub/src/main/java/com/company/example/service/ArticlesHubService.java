package com.company.example.service;

import com.company.example.api.FluxArticleSourceClient;
import com.company.example.model.Article;
import com.company.example.model.PublisherInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;


@SuppressWarnings("ALL")
@Slf4j
@Service
@RequiredArgsConstructor
public class ArticlesHubService implements ArticlesService {

    private final List<FluxArticleSourceClient> articleSourceClients;

    @Override
    public Flux<Article> loadArticles(PublisherInfo publisherInfo) {
        List<Flux<Article>> collect = articleSourceClients.stream()
            .map(client -> getArticles(publisherInfo, client))
            .collect(Collectors.toList());
        return Flux.merge(collect);
    }

    private Flux<Article> getArticles(PublisherInfo publisherInfo, FluxArticleSourceClient client) {
        return client.getArticlesByPublisher(publisherInfo.getName())
            .onErrorResume(err -> Flux.empty())
            .timeout(Duration.ofSeconds(30));
//            .take(Duration.ofSeconds(6));
    }

}
