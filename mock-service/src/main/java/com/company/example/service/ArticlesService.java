package com.company.example.service;

import com.company.example.model.Article;
import com.company.example.tool.ArticlesGenerator;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.Random;

@Service
public class ArticlesService {

    @Value("${mock.delay.max}")
    private int maxDelay;

    private static final List<Article> articles;

    static {
        articles = ArticlesGenerator.generate(303, "Mock");
    }

    @SneakyThrows
    public Flux<Article> getArticles() {
        return Flux.fromIterable(articles)
            .delayElements(Duration.ofMillis(new Random().nextInt(maxDelay)));
    }

    @SneakyThrows
    public Flux<Article> getArticlesByPublisher(String publisherId) {
        return Flux.fromStream(
            articles.stream()
                .filter(article -> article.getPublisherId().equals(publisherId))
        ).delayElements(Duration.ofMillis(new Random().nextInt(maxDelay)));
    }

}
