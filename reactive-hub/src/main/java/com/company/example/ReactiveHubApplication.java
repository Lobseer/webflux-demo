package com.company.example;

import com.company.example.api.FluxArticleSourceClient;
import com.company.example.client.AwsS3ServiceClient;
import com.company.example.client.MockServiceClient;
import com.company.example.client.MongoServiceClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

import static java.util.Arrays.asList;


@SpringBootApplication
public class ReactiveHubApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveHubApplication.class, args);
    }

    @Bean
    public List<FluxArticleSourceClient> articleSourceClients(MockServiceClient mockServiceClient,
                                                              MongoServiceClient mongoServiceClient,
                                                              AwsS3ServiceClient awsS3ServiceClient) {
        return asList(awsS3ServiceClient, mongoServiceClient , mockServiceClient);
    }

}
