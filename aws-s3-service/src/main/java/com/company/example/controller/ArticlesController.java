package com.company.example.controller;

import com.company.example.api.FluxArticleSourceClient;
import com.company.example.model.Article;
import com.company.example.service.S3FileService;
import com.company.example.tool.ArticlesGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;


@RestController
@RequestMapping("/articles")
public class ArticlesController implements FluxArticleSourceClient<Article> {

    private final S3FileService s3FileService;

    @Value("${aws.s3.bucket.name}")
    private String bucketName;
    @Value("${aws.s3.bucket.article.folder}")
    private String articleFolder;

    @Autowired
    public ArticlesController(S3FileService s3FileService) {
        this.s3FileService = s3FileService;
    }

    @GetMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Article> getArticles() {
        return s3FileService.getAllFiles(bucketName, articleFolder, Article.class);
    }

    @GetMapping(value = "/{publisherId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Article> getArticlesByPublisher(@PathVariable String publisherId) {
        return s3FileService.getAllFiles(bucketName, articleFolder, Article.class)
            .filter(article -> article.getPublisherId().equals(publisherId));
    }

    @GetMapping("/setUp")
    public void setUp() {
        String filePath = articleFolder + "/%s.json";
        AtomicInteger i = new AtomicInteger();
        for (Article article : ArticlesGenerator.generate(101, "S3")) {
            String fileKey = String.format(filePath, "article" + i.getAndIncrement());
            s3FileService.uploadObject(fileKey, bucketName, article);
        }
    }

}
