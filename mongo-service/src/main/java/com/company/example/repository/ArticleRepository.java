package com.company.example.repository;

import com.company.example.entity.ArticleEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ArticleRepository extends ReactiveMongoRepository<ArticleEntity, String> {

    Flux<ArticleEntity> findAllByPublisherId(String publisherId);

}
