package com.company.example.controller;

import com.company.example.api.FluxArticleSourceClient;
import com.company.example.model.Article;
import com.company.example.service.ArticlesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RequestMapping("/articles")
@RestController
@RequiredArgsConstructor
public class ArticlesController implements FluxArticleSourceClient<Article> {

    private final ArticlesService articlesService;

    @GetMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Article> getArticles() {
        return articlesService.getArticles();
    }

    @GetMapping(value = "/{publisherId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Article> getArticlesByPublisher(@PathVariable("publisherId") String publisherId) {
        return articlesService.getArticlesByPublisher(publisherId);
    }

}
