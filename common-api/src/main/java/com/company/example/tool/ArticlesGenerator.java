package com.company.example.tool;

import com.company.example.model.Article;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArticlesGenerator {

    public static List<Article> generate(int amount, String prefix) {
        List<Article> result = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Article newArticle = Article.builder()
                .title(prefix + " Title = " + i)
                .description(prefix + " Description = " + i)
                .publisherId(getPublisher())
                .build();
            result.add(newArticle);
        }
        return result;
    }

    private static String getPublisher() {
        return "P" + new Random().nextInt(5);
    }

}
