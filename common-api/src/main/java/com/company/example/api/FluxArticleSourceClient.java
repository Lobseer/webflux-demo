package com.company.example.api;

import com.company.example.model.Article;
import reactor.core.publisher.Flux;

public interface FluxArticleSourceClient<T extends Article> {

    Flux<T> getArticles();

    Flux<T> getArticlesByPublisher(String publisherId);

}
