package com.company.example.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.core.BytesWrapper;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;

@Service
public class S3FileService {

    private static final String S3_KEY_DELIMITER = "/";

    private final S3AsyncClient s3Client;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public S3FileService(S3AsyncClient s3Client) {
        this.s3Client = s3Client;
    }

    public <T> Flux<T> getAllFiles(String bucket, String folder, Class<T> modelClass) {
        Flux<String> fileKeysStream = getFileKeysStream(bucket, folder);

        return fileKeysStream
            .flatMap(fileName -> getFileByKey(fileName, bucket, modelClass));
    }

    @SneakyThrows
    private <T> Mono<T> getFileByKey(String fileName, String bucket, Class<T> modelClass) {
        GetObjectRequest request = GetObjectRequest.builder()
            .bucket(bucket)
            .key(fileName)
            .build();
        CompletableFuture<ResponseBytes<GetObjectResponse>> object = s3Client.getObject(request, AsyncResponseTransformer.toBytes());
        return Mono.fromFuture(object)
            .map(BytesWrapper::asInputStream)
            .map(inputStream -> mapToObject(inputStream, modelClass));
    }

    private <T> T mapToObject(java.io.InputStream inputStream, Class<T> modelClass) {
        try {
            return objectMapper.readValue(inputStream, modelClass);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Unable to map File to %s error", modelClass.getSimpleName()), e);
        }
    }

    private Flux<String> getFileKeysStream(String bucket, String folder) {
        ListObjectsRequest request = ListObjectsRequest.builder()
            .bucket(bucket)
            .prefix(folder + S3_KEY_DELIMITER)
            .delimiter(S3_KEY_DELIMITER)
            .build();
        return Mono.fromFuture(s3Client.listObjects(request))
            .map(ListObjectsResponse::contents)
            .flatMapIterable(list -> list)
            .map(S3Object::key)
            .filter(Pattern.compile(".+\\..+").asPredicate());
    }

    @SneakyThrows
    public <T> void uploadObject(String fileName, String bucket, T obj) {
        PutObjectRequest putRequest = PutObjectRequest.builder()
            .bucket(bucket)
            .key(fileName)
            .acl(ObjectCannedACL.PUBLIC_READ)
            .build();
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        AsyncRequestBody requestBody = AsyncRequestBody.fromString(json);
        s3Client.putObject(putRequest, requestBody);
    }

}
