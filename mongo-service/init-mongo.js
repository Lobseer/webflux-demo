
//Note: The code in the docker-entrypoint-init.d folder is only executed if the database has never been initialized before.

db.createUser(
    {
        user: "admin",
        pwd: "1234",
        roles: [
            {
                role: "readWrite",
                db: "testdb"
            }
        ]
    }
)

function populateDb(dbName, colName, num) {

    let col = db.getCollection(colName);
    let bulk = col.initializeUnorderedBulkOp();

    for (let i = 0; i < num; i++) {
        bulk.insert({
            title: 'Mongo Title = ' + i,
            description: 'Mongo Description = ' + i,
            publisherId: 'P' + Math.floor(Math.random() * 5)
        });
    }
    bulk.execute();
}

populateDb('testdb', 'articles', 305);
