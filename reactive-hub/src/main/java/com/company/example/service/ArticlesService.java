package com.company.example.service;

import com.company.example.model.Article;
import com.company.example.model.PublisherInfo;
import reactor.core.publisher.Flux;

public interface ArticlesService {

    Flux<Article> loadArticles(PublisherInfo publisherInfo);

}
