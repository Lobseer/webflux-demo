package com.company.example.api;

import com.company.example.model.Article;

import java.util.List;

public interface ArticleSourceClient<T extends Article> {

    List<T> getArticles();

    List<T> getArticlesByPublisher(String publisherId);

}
