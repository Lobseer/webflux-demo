package com.company.example

import io.gatling.core.Predef._
import io.gatling.core.feeder.{FeederBuilderBase, SourceFeederBuilder}
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

class HubLoadSimulation extends Simulation {

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl(TestConfig.baseUrl + TestConfig.basePath)

  private val dataFeeder: FeederBuilderBase[String] = Array(
    Map("publisherId" -> "P0"),
    Map("publisherId" -> "P1"),
    Map("publisherId" -> "P2"),
    Map("publisherId" -> "P3"),
    Map("publisherId" -> "P4")
  ).random.circular

  val chain: ChainBuilder = feed(dataFeeder).
    exec(http("Get ALL articles by publisher")
      .get("/hub/articles/${publisherId}")
      .check(status.is(200))
    ).pause(10)

  val scenario_get_sheets: ScenarioBuilder = scenario("Get ALL articles")
    .during(TestConfig.loadTimeSeconds) {
      exec(chain)
    }

  setUp(
    scenario_get_sheets.inject(
      rampUsers(TestConfig.maxUsers) during TestConfig.rampTimeSeconds.seconds,
      nothingFor(TestConfig.loadTimeSeconds.seconds)
    )
  )
    .maxDuration(TestConfig.maxLoadTime.seconds)
    .protocols(httpProtocol)

}
