package com.company.example.controller;

import com.company.example.api.FluxArticleSourceClient;
import com.company.example.entity.ArticleEntity;
import com.company.example.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Random;

@RequestMapping("/articles")
@RestController
@RequiredArgsConstructor
public class ArticlesController implements FluxArticleSourceClient<ArticleEntity> {

    private final ArticleRepository articleRepository;

    @Value("${mock.delay.max}")
    private int maxDelay;

    @GetMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<ArticleEntity> getArticles() {
        return articleRepository.findAll()
            .delayElements(Duration.ofMillis(new Random().nextInt(maxDelay)));
    }

    @GetMapping(value = "/{publisherId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<ArticleEntity> getArticlesByPublisher(@PathVariable("publisherId") String publisherId) {
        return articleRepository.findAllByPublisherId(publisherId)
            .delayElements(Duration.ofMillis(new Random().nextInt(maxDelay)));
    }

}
