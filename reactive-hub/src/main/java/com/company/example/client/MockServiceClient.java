package com.company.example.client;

import com.company.example.api.FluxArticleSourceClient;
import com.company.example.model.Article;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class MockServiceClient extends BaseClient implements FluxArticleSourceClient<Article> {

    public MockServiceClient(@Value("${services.mock.url}") String serviceUrl) {
        super(serviceUrl);
    }

    @Override
    public Flux<Article> getArticles() {
        return this.getClient("articles", MediaType.APPLICATION_STREAM_JSON)
            .flatMapMany(res -> res.bodyToFlux(Article.class));
    }

    @Override
    public Flux<Article> getArticlesByPublisher(String publisherId) {
        return this.getClient("articles/" + publisherId, MediaType.APPLICATION_STREAM_JSON)
            .flatMapMany(res -> res.bodyToFlux(Article.class));
    }

}
