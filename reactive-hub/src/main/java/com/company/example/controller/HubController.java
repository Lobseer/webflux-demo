package com.company.example.controller;

import com.company.example.model.Article;
import com.company.example.model.PublisherInfo;
import com.company.example.service.ArticlesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("hub")
@RequiredArgsConstructor
public class HubController {

    private final ArticlesService articlesService;

    @RequestMapping(method = GET, value = "/articles/{publisherId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Article> loadCards(@PathVariable("publisherId") String publisherId) {
        return articlesService.loadArticles(PublisherInfo.builder().name(publisherId).build());
    }

}
